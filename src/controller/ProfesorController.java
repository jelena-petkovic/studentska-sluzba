package controller;

import model.BazaProfesora;
import model.Profesor;
import view.Entiteti;

public class ProfesorController {

	private static ProfesorController instance = null;
	
	public static ProfesorController getInstance() {
		if (instance == null) {
			instance = new ProfesorController();
		}
		return instance;
	}
	
	private ProfesorController() {}

	public void dodajProfesora(Profesor p) {

		BazaProfesora.getInstance().add(p);
		Entiteti.getInstance().azurirajPrikazProfesor();
	}
	
    public void izmeniProfesora(int row, Profesor p) {
    	
    	BazaProfesora.getInstance().setProfesor(row, p);
		Entiteti.getInstance().azurirajPrikazProfesor();		    	
    	
    }
    
    public void izbrisiProfesora() {
    	
		int row = Entiteti.getInstance().getSelectedRowProfesor();
    	BazaProfesora.getInstance().remove(row);
		Entiteti.getInstance().azurirajPrikazProfesor();
    }
	
}
