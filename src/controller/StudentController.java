package controller;

import model.BazaStudenata;
import model.Student;
import view.Entiteti;

public class StudentController {
	
	private static StudentController instance = null;
	
	public static StudentController getInstance() {
		if (instance == null) {
			instance = new StudentController();
		}
		return instance;
	}
	
	private StudentController() {}

	public void dodajStudenta(Student s) {

		BazaStudenata.getInstance().add(s);
		Entiteti.getInstance().azurirajPrikaz();
	}
	
    public void izmeniStudenta(int row, Student s) {
    	
    	BazaStudenata.getInstance().replace(row, s);
    	Entiteti.getInstance().azurirajPrikaz();		    	
    	
    }
    
    public void izbrisiStudenta() {
    	int row = Entiteti.getInstance().getSelectedRowStudent();
		BazaStudenata.getInstance().remove(row);
		Entiteti.getInstance().azurirajPrikaz();
    }
}
