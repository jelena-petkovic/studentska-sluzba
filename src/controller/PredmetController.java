package controller;

import model.BazaPredmeta;
import model.Predmet;
import model.Profesor;
import model.Student;
import view.Entiteti;

public class PredmetController {

	private static PredmetController instance = null;
	
	public static PredmetController getInstance() {
		if (instance == null) {
			instance = new PredmetController();
		}
		return instance;
	}
	
	private PredmetController() {}

	public void dodajPredmet(Predmet p) {

		BazaPredmeta.getInstance().add(p);
		Entiteti.getInstance().azurirajPrikazPredmet();
	}
	
    public void izmeniPredmet(int row, Predmet p) {
    	
    	BazaPredmeta.getInstance().setPredmet(row, p);
		Entiteti.getInstance().azurirajPrikazPredmet();		    	
    	
    }
    
    public void izbrisiPredmet() {
    	
		int row = Entiteti.getInstance().getSelectedRowPredmet();
    	BazaPredmeta.getInstance().remove(row);
		Entiteti.getInstance().azurirajPrikazPredmet();
    }
    
    public void dodajStudenta(Student s, Predmet p) {
    	p.addStudent(s);
		Entiteti.getInstance().azurirajPrikazPredmet();
    }
    
    public void dodajProfesora(Profesor p, Predmet pr) {
    	pr.addProfesor(p);
		Entiteti.getInstance().azurirajPrikazPredmet();
    }
}
