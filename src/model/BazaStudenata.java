package model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;







public class BazaStudenata {
	
	private static BazaStudenata instance = null;

	public static BazaStudenata getInstance() {
		if (instance == null) {
			instance = new BazaStudenata();
		}
		return instance;
		
	}
	
	private ArrayList<Student> data = new ArrayList<Student>();
	private ArrayList<Student> temp = new ArrayList<Student>();
	
	private BazaStudenata() {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");

		
		data.add(new Student( "Milan", "Vidakovi\u0107", LocalDate.parse("1/07/1990", formatter),"Adresa","telefon","email","RA1112015", LocalDate.parse("1/07/2018", formatter)
		,1,Status.B,9.81));
		
		data.add(new Student( "Branko", "Milosavljevi\u0107", LocalDate.parse("1/07/1990", formatter),"Adresa","telefon","email","RA1112016", LocalDate.parse("1/07/2018", formatter)
		,1,Status.B,9.81));
		
		data.add(new Student( "Petar", "Petrovi\u0107", LocalDate.parse("1/07/1990", formatter),"Adresa","telefon","email","RA1112017", LocalDate.parse("1/07/2018", formatter)
		,2,Status.B,9.81));
		
		data.add(new Student( "Marko", "Markovi\u0107", LocalDate.parse("1/07/1990", formatter),"Adresa","telefon","email","RA1112018", LocalDate.parse("1/07/2018", formatter)
		,3,Status.S
		
		,9.81));
		
		temp = new ArrayList<Student>(data);
		
		

	}
	

	
	@Override
	public String toString() {
		return "BazaStudenata [data=" + data + ", temp=" + temp + "]";
	}



	public Student getStudent(int i) {
		return data.get(i);
	}
	
	public Student getStudent(String indeksStudenta) {
		
		Student student = null;
		
		for ( int i = 0 ; i < data.size() ; i++ ) {
			Student s = data.get(i);
			if ( s.getBroj_indeksa().equals(indeksStudenta)) {
				student = s;
				break;
			}
				
		}
		return student;
	}
	
	public void setStudent(int i,Student s) {
		Student stari = getStudent(i);
		int j;
		for ( j=0;j<temp.size();j++ ) {
			if ( stari.getBroj_indeksa() == temp.get(j).getBroj_indeksa() ) {
				break;
			}
		}
		
		temp.set(j, s);
		data.set(i, s);
		
	}
	
	
	public int size() {
		return data.size();
	}
	
	public void add(Student s) {
		data.add(s);
		temp.add(s);
	}
	
	
	public void remove(int row) {
		
		int j;
		for ( j=0;j<temp.size();j++ ) {
			if ( data.get(row).getBroj_indeksa() == temp.get(j).getBroj_indeksa() ) {
				break;
			}
		}
		
		temp.remove(j);
		data.remove(row);
		
	}
	public void replace(int i, Student s) {
		Student stari = getStudent(i);
		int j;
		for ( j=0;j<temp.size();j++ ) {
			if ( stari.getBroj_indeksa() == temp.get(j).getBroj_indeksa() ) {
				break;
			}
		}
		
		temp.set(j, s);
		data.set(i, s);
	}
	
	public ArrayList<Student> getData(){
		return data;
	}
	
	public ArrayList<Student> getTemp(){
		return temp;
	}
	
	public void refresh() {
		data = new ArrayList<Student>();
	}
	public void suitable(Student s) {
		data.add(s);
	}
}
