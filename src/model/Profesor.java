package model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Profesor extends Osoba {
	
	private String adresa_kancelarije;
	private String br_licne_karte;
	private Titula titula;
	private String zvanje;
	private ArrayList<String> spisak_predmeta_pr;
	
	public Profesor() {
		super();
		adresa_kancelarije = "";
		br_licne_karte = "";
		titula = titula.T1;
		zvanje = "";
		spisak_predmeta_pr = new ArrayList<String>();
	}
	
	public Profesor(String ime, String prezime, LocalDate datum_rodjenja, String adresa_stanovanja, String kontakt_telefon,
			String email_adresa, String adresa_kancelarije, String br_licne_karte, Titula titula, String zvanje,
			ArrayList<String> spisak_predmeta_pr) {
		super(ime,prezime,datum_rodjenja,adresa_stanovanja,kontakt_telefon,email_adresa);
		this.adresa_kancelarije = adresa_kancelarije;
		this.br_licne_karte = br_licne_karte;
		this.titula = titula;
		this.zvanje = zvanje;
		this.spisak_predmeta_pr = spisak_predmeta_pr;
	}
	
	public Profesor(String ime, String prezime, LocalDate datum_rodjenja, String adresa_stanovanja, String kontakt_telefon,
			String adresa_kancelarije, String br_licne_karte, Titula titula, String zvanje) {
		super(ime,prezime,datum_rodjenja,adresa_stanovanja,kontakt_telefon,ime+prezime+"@gmail.com");
		this.adresa_kancelarije = adresa_kancelarije;
		this.br_licne_karte = br_licne_karte;
		this.titula = titula;
		this.zvanje = zvanje;
		spisak_predmeta_pr = null;
	}
	
	public Profesor(Profesor p) {
		super((Osoba)p);
		this.adresa_kancelarije = p.adresa_kancelarije;
		this.br_licne_karte = p.br_licne_karte;
		this.titula = p.titula;
		this.zvanje = p.zvanje;
		this.spisak_predmeta_pr = p.spisak_predmeta_pr;
	}


	public String getAdresa_kancelarije() {
		return adresa_kancelarije;
	}

	public void setAdresa_kancelarije(String adresa_kancelarije) {
		this.adresa_kancelarije = adresa_kancelarije;
	}

	public String getBr_licne_karte() {
		return br_licne_karte;
	}

	public void setBr_licne_karte(String br_licne_karte) {
		this.br_licne_karte = br_licne_karte;
	}

	public Titula getTitula() {
		return titula;
	}

	public void setTitula(Titula titula) {
		this.titula = titula;
	}

	public String getZvanje() {
		return zvanje;
	}

	public void setZvanje(String zvanje) {
		this.zvanje = zvanje;
	}

	public ArrayList<String> getSpisak_predmeta_pr() {
		return spisak_predmeta_pr;
	}

	public void setSpisak_predmeta_pr(ArrayList<String> spisak_predmeta_pr) {
		this.spisak_predmeta_pr = spisak_predmeta_pr;
	}
	
	

	
	
	
	public Object toCell(int col) {
		switch(col) {
		case 0: return ime;
		case 1: return prezime;
		case 2: return datum_rodjenja;
		case 3: return adresa_stanovanja;
		case 4: return kontakt_telefon;
		case 5: return email_adresa;
		case 6: return adresa_kancelarije;
		case 7: return br_licne_karte;
		case 8: return titula;
		case 9: return zvanje;
		default: return null;
		}
	}
	//doradi
	public void set(int col, Object value) {
		switch(col) {
		case 0: ime = (String)value;
		break;
		case 1: prezime = (String)value;
		break;

	}
		
	}

	@Override
	public String toString() {
		return this.getIme()+" "+this.getPrezime();
	}
}
