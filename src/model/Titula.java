package model;

public enum Titula {
	T1("titula1"), T2("titula2");
	String titula;
	private Titula(){};
	private Titula(String titula) { this.titula = titula; }

	
	public String getStatus() {
		return titula;
	}
}
