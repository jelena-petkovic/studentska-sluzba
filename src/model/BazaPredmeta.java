package model;

import java.util.ArrayList;

public class BazaPredmeta {

	private ArrayList<Predmet> data = new ArrayList<Predmet>();
	private ArrayList<Predmet> temp = new ArrayList<Predmet>();
	private static BazaPredmeta instance = null;
	
	private BazaPredmeta() {
			
		data.add(new Predmet( "04980", "Analiza", "prvi", "1", new Profesor(),new ArrayList<Student>()));
		data.add(new Predmet( "06580", "Algebra", "prvi", "2", new Profesor(),new ArrayList<Student>()));
		data.add(new Predmet( "06480", "Programirane", "prvi", "3", new Profesor(),new ArrayList<Student>()));
		data.add(new Predmet( "04520", "Analiza", "prvi", "4", new Profesor(),new ArrayList<Student>()));
		temp = new ArrayList<Predmet>(data);
	}
	

	
	public Predmet getPredmet(int i) {
		return data.get(i);
	}
	
	public void setPredmet(int i,Predmet p) {
		Predmet stari = getPredmet(i);
		int j;
		for ( j=0;j<temp.size();j++ ) {
			if ( stari.getSifra_predmeta() == temp.get(j).getSifra_predmeta() ) {
				break;
			}
		}
		
		temp.set(j, p);
		data.set(i, p);
	}
	
	public static BazaPredmeta getInstance() {
		if (instance == null) {
			instance = new BazaPredmeta();
		}
		return instance;
		
	}
	
	public int size() {
		return data.size();
	}
	
	public void add(Predmet p) {
		data.add(p);
		temp.add(p);
	}
	
	public void remove(int row) {
		int j;
		for ( j=0;j<temp.size();j++ ) {
			if ( data.get(row).getSifra_predmeta() == temp.get(j).getSifra_predmeta() ) {
				break;
			}
		}
		
		temp.remove(j);
		data.remove(row);
	}
	public void replace(int row, Predmet p) {
		Predmet stari = getPredmet(row);
		int j;
		for ( j=0;j<temp.size();j++ ) {
			if ( stari.getSifra_predmeta() == temp.get(j).getSifra_predmeta() ) {
				break;
			}
		}
		
		temp.set(j, p);
		data.set(row, p);
	}
	public int getLength() {
		return data.size();
	}
	
	public ArrayList<Predmet> getData(){
		return data;
	}
	
	public ArrayList<Predmet> getTemp(){
		return temp;
	}
	
	public void refresh() {
		data = new ArrayList<Predmet>();
	}
	public void suitable(Predmet s) {
		data.add(s);
	}
}
