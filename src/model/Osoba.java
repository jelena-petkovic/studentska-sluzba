package model;

import java.time.LocalDate;

abstract class Osoba {
	
	protected String ime;
	protected String prezime;
	protected LocalDate datum_rodjenja;
	protected String adresa_stanovanja;
	protected String kontakt_telefon;
	protected String email_adresa;
	
	public Osoba() {
		this.ime = "";
		this.prezime = "";
		this.datum_rodjenja = LocalDate.now();
		this.adresa_stanovanja = "";
		this.kontakt_telefon = "";
		this.email_adresa = "";
	}
	
	public Osoba(String ime, String prezime, LocalDate datum_rodjenja, String adresa_stanovanja, String kontakt_telefon,
			String email_adresa) {
		this.ime = ime;
		this.prezime = prezime;
		this.datum_rodjenja = datum_rodjenja;
		this.adresa_stanovanja = adresa_stanovanja;
		this.kontakt_telefon = kontakt_telefon;
		this.email_adresa = email_adresa;
	}
	
	public Osoba(Osoba o) {
		this.ime = o.ime;
		this.prezime = o.prezime;
		this.datum_rodjenja = o.datum_rodjenja;
		this.adresa_stanovanja = o.adresa_stanovanja;
		this.kontakt_telefon = o.kontakt_telefon;
		this.email_adresa = o.email_adresa;
	}
	
	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public LocalDate getDatum_rodjenja() {
		return datum_rodjenja;
	}

	public void setDatum_rodjenja(LocalDate datum_rodjenja) {
		this.datum_rodjenja = datum_rodjenja;
	}

	public String getAdresa_stanovanja() {
		return adresa_stanovanja;
	}

	public void setAdresa_stanovanja(String adresa_stanovanja) {
		this.adresa_stanovanja = adresa_stanovanja;
	}

	public String getKontakt_telefon() {
		return kontakt_telefon;
	}

	public void setKontakt_telefon(String kontakt_telefon) {
		this.kontakt_telefon = kontakt_telefon;
	}

	public String getEmail_adresa() {
		return email_adresa;
	}

	public void setEmail_adresa(String email_adresa) {
		this.email_adresa = email_adresa;
	}
	
}
