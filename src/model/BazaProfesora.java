package model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class BazaProfesora {

	private ArrayList<Profesor> data = new ArrayList<Profesor>();
	private ArrayList<Profesor> temp = new ArrayList<Profesor>();
	private static BazaProfesora instance = null;
	
	private BazaProfesora() {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");

		
		data.add(new Profesor( "Ivan", "Vidakovi\u0107", LocalDate.parse("1/07/1990", formatter),"Adresa","telefon","adresa kancelarije","012345679",Titula.T1,"Zvanje"));
		
		data.add(new Profesor( "Sava", "Savi\u0107", LocalDate.parse("1/07/1990", formatter),"Adresa","telefon","adresa kancelarije","012345679",Titula.T1,"Zvanje"));
		
		data.add(new Profesor( "Jovan", "Jovi\u0107", LocalDate.parse("1/07/1990", formatter),"Adresa","telefon","adresa kancelarije","012345679",Titula.T1,"Zvanje"));
		
		data.add(new Profesor( "Milan", "Mili\u0107", LocalDate.parse("1/07/1990", formatter),"Adresa","telefon","adresa kancelarije","012345679",Titula.T1,"Zvanje"));
		
		temp = new ArrayList<Profesor>(data);
		
	}
	

	
	public Profesor getProfesor(int i) {
		return data.get(i);
	}
	
	public Profesor getProfesor(String brL) {
		//mozda temp ovde
		Profesor profesor = null;
		
		for ( int i = 0 ; i < data.size() ; i++ ) {
			Profesor p = data.get(i);
			if ( p.getBr_licne_karte().equals(brL)) {
				profesor = p;
				break;
			}
				
		}
		return profesor;
	}
	
	public void setProfesor(int i,Profesor p) {
		Profesor stari = getProfesor(i);
		int j;
		for ( j=0;j<temp.size();j++ ) {
			if ( stari.getBr_licne_karte() == temp.get(j).getBr_licne_karte() ) {
				break;
			}
		}
		
		temp.set(j, p);
		data.set(i, p);
	}
	
	public static BazaProfesora getInstance() {
		if (instance == null) {
			instance = new BazaProfesora();
		}
		return instance;
		
	}
	
	public int size() {
		return data.size();
	}
	
	public void add(Profesor p) {
		data.add(p);
		temp.add(p);
	}
	
	public void remove(int row) {
		int j;
		for ( j=0;j<temp.size();j++ ) {
			if ( data.get(row).getBr_licne_karte() == temp.get(j).getBr_licne_karte() ) {
				break;
			}
		}
		
		temp.remove(j);
		data.remove(row);
	}
	public void replace(int row, Profesor p) {
		Profesor stari = getProfesor(row);
		int j;
		for ( j=0;j<temp.size();j++ ) {
			if ( stari.getBr_licne_karte() == temp.get(j).getBr_licne_karte() ) {
				break;
			}
		}
		
		temp.set(j, p);
		data.set(row, p);
	}
	
	public ArrayList<Profesor> getData(){
		return data;
	}
	
	public ArrayList<Profesor> getTemp(){
		return temp;
	}
	
	public void refresh() {
		data = new ArrayList<Profesor>();
	}
	public void suitable(Profesor s) {
		data.add(s);
	}
	
	
}
