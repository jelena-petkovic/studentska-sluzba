package model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;


public class Student extends Osoba {
	
	private String broj_indeksa;
	private LocalDate datum_upisa;
	private int trenutna_godina_studija;
	private Status status;
	private double prosecna_ocena;
	private ArrayList<String> spisak_predmeta;
	
	public Student() {
		super();
		this.broj_indeksa = "";
		this.datum_upisa = LocalDate.now();
		this.trenutna_godina_studija = 0;
		this.status = status.B;
		this.prosecna_ocena = 0;
		this.spisak_predmeta = new ArrayList<String>();
	}

	public Student(String ime, String prezime, LocalDate datum_rodjenja, String adresa_stanovanja, String kontakt_telefon,
			String email_adresa, String broj_indeksa, LocalDate datum_upisa, int trenutna_godina_studija, Status status,
			double prosecna_ocena, ArrayList<String> spisak_predmeta) {
		super(ime,prezime,datum_rodjenja,adresa_stanovanja,kontakt_telefon,email_adresa);
		this.broj_indeksa = broj_indeksa;
		this.datum_upisa = datum_upisa;
		this.trenutna_godina_studija = trenutna_godina_studija;
		this.status = status;
		this.prosecna_ocena = prosecna_ocena;
		this.spisak_predmeta = spisak_predmeta;
	}
	
	public Student(String ime, String prezime, LocalDate datum_rodjenja, String adresa_stanovanja, String kontakt_telefon,
			String email_adresa, String broj_indeksa, LocalDate datum_upisa, int trenutna_godina_studija, Status status,
			double prosecna_ocena) {
		super(ime,prezime,datum_rodjenja,adresa_stanovanja,kontakt_telefon,email_adresa);
		this.broj_indeksa = broj_indeksa;
		this.datum_upisa = datum_upisa;
		this.trenutna_godina_studija = trenutna_godina_studija;
		this.status = status;
		this.prosecna_ocena = prosecna_ocena;
		spisak_predmeta=null;
	}
	
	public Student(String ime, String prezime, LocalDate datum_rodjenja, String adresa_stanovanja, String kontakt_telefon,
			 String broj_indeksa, int trenutna_godina_studija, Status status) {
		super(ime,prezime,datum_rodjenja,adresa_stanovanja,kontakt_telefon,ime+prezime+"@gmail.com");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
		this.broj_indeksa = broj_indeksa;
		this.datum_upisa = LocalDate.parse("1/07/1990", formatter);
		this.trenutna_godina_studija = trenutna_godina_studija;
		this.status = status;
		this.prosecna_ocena = 0;
		spisak_predmeta=null;
	}
	
	
	public Student(Student s) {
		super((Osoba)s);
		this.broj_indeksa = s.broj_indeksa;
		this.datum_upisa = datum_upisa;
		this.trenutna_godina_studija = trenutna_godina_studija;
		this.status = status;
		this.prosecna_ocena = prosecna_ocena;
		this.spisak_predmeta = spisak_predmeta;
	}

	public String getBroj_indeksa() {
		return broj_indeksa;
	}

	public void setBroj_indeksa(String broj_indeksa) {
		this.broj_indeksa = broj_indeksa;
	}

	public LocalDate getDatum_upisa() {
		return datum_upisa;
	}

	public void setDatum_upisa(LocalDate datum_upisa) {
		this.datum_upisa = datum_upisa;
	}

	public int getTrenutna_godina_studija() {
		return trenutna_godina_studija;
	}

	public void setTrenutna_godina_studija(int trenutna_godina_studija) {
		this.trenutna_godina_studija = trenutna_godina_studija;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public double getProsecna_ocena() {
		return prosecna_ocena;
	}

	public void setProsecna_ocena(double prosecna_ocena) {
		this.prosecna_ocena = prosecna_ocena;
	}

	public ArrayList<String> getSpisak_predmeta() {
		return spisak_predmeta;
	}

	public void setSpisak_predmeta(ArrayList<String> spisak_predmeta) {
		this.spisak_predmeta = spisak_predmeta;
	}
	
	

	
	
	
	
	
	public Object toCell(int col) {
		switch(col) {
		case 0: return ime;
		case 1: return prezime;
		case 2: return datum_rodjenja;
		case 3: return adresa_stanovanja;
		case 4: return kontakt_telefon;
		case 5: return email_adresa;
		case 6: return broj_indeksa;
		case 7: return datum_upisa;
		case 8: return trenutna_godina_studija;
		case 9: return status;
		case 10: return prosecna_ocena;
		default: return null;
		}
	}
	//doradi
	public void set(int col, Object value) {
		switch(col) {
		case 0: ime = (String)value;
		break;
		case 1: prezime = (String)value;
		break;

	}
	
	}
	
}
