package model;

import java.util.ArrayList;

public class Predmet {
	private String sifra_predmeta;
	private String naziv_predmeta;
	private String semestar; 			//moze enum
	private String god_studija;
	private Profesor predmetni_profesor;
	private ArrayList<Student> spisak_studenata_koji_slusaju;
	
	public Predmet() {
		sifra_predmeta = "";
		naziv_predmeta = "";
		semestar = "";
		god_studija = "";
		predmetni_profesor = new Profesor();
		spisak_studenata_koji_slusaju = new ArrayList<Student>();
	}
	
	public Predmet(String sifra_predmeta, String naziv_predmeta, String semestar, String god_studija,
			Profesor predmetni_profesor, ArrayList<Student> spisak_studenata_koji_slusaju) {
		this.sifra_predmeta = sifra_predmeta;
		this.naziv_predmeta = naziv_predmeta;
		this.semestar = semestar;
		this.god_studija = god_studija;
		this.predmetni_profesor = predmetni_profesor;
		this.spisak_studenata_koji_slusaju = spisak_studenata_koji_slusaju;
	}
	
	
	public Predmet(String sifra_predmeta, String naziv_predmeta, String semestar, String god_studija,Profesor p) {
		this.sifra_predmeta = sifra_predmeta;
		this.naziv_predmeta = naziv_predmeta;
		this.semestar = semestar;
		this.god_studija = god_studija;
		this.predmetni_profesor = p;
		this.spisak_studenata_koji_slusaju = new ArrayList<Student>();
	}
	
	public Predmet(String sifra_predmeta, String naziv_predmeta, String semestar, String god_studija) {
		this.sifra_predmeta = sifra_predmeta;
		this.naziv_predmeta = naziv_predmeta;
		this.semestar = semestar;
		this.god_studija = god_studija;
		this.predmetni_profesor = null;
		this.spisak_studenata_koji_slusaju = new ArrayList<Student>();;
	}
	
	public Predmet(Predmet p) {
		this.sifra_predmeta = p.sifra_predmeta;
		this.naziv_predmeta = p.naziv_predmeta;
		this.semestar = p.semestar;
		this.god_studija = p.god_studija;
		this.predmetni_profesor = p.predmetni_profesor;
		this.spisak_studenata_koji_slusaju = p.spisak_studenata_koji_slusaju;
	}
	
	
	public String getSifra_predmeta() {
		return sifra_predmeta;
	}
	public void setSifra_predmeta(String sifra_predmeta) {
		this.sifra_predmeta = sifra_predmeta;
	}
	public String getNaziv_predmeta() {
		return naziv_predmeta;
	}
	public void setNaziv_predmeta(String naziv_predmeta) {
		this.naziv_predmeta = naziv_predmeta;
	}
	public String getSemestar() {
		return semestar;
	}
	public void setSemestar(String semestar) {
		this.semestar = semestar;
	}
	public String getGod_studija() {
		return god_studija;
	}
	public void setGod_studija(String god_studija) {
		this.god_studija = god_studija;
	}
	public Profesor getPredmetni_profesor() {
		return predmetni_profesor;
	}
	public void setPredmetni_profesor(Profesor predmetni_profesor) {
		this.predmetni_profesor = predmetni_profesor;
	}
	public ArrayList<Student> getSpisak_studenata_koji_slusaju() {
		return spisak_studenata_koji_slusaju;
	}
	public void setSpisak_studenata_koji_slusaju(ArrayList<Student> spisak_studenata_koji_slusaju) {
		this.spisak_studenata_koji_slusaju = spisak_studenata_koji_slusaju;
	}
	
	public Object toCell(int col) {
		switch(col) {
		case 0: return sifra_predmeta;
		case 1: return naziv_predmeta;
		case 2: return semestar;
		case 3: return god_studija;
		case 4: return predmetni_profesor;
		case 5: return "prikazi";
		default: return null;
		}
	}

	
	//doradi
	public void set(int col, Object value) {
		switch(col) {
		case 0: sifra_predmeta = (String)value;
		break;
		case 1: naziv_predmeta = (String)value;
		break;

	}
	
	}
	
	public void addStudent(Student s) {
		spisak_studenata_koji_slusaju.add(s);
	}
	
	public void addProfesor(Profesor p) {
		predmetni_profesor = p;
	}
	public void deleteStudent(int i) {
		spisak_studenata_koji_slusaju.remove(i);
	}

}
