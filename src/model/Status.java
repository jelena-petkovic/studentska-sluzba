package model;

public enum Status {

	B("Budzet"), S("Samofinansiranje");
	String status;
	private Status(){};
	private Status(String status) { this.status = status; }

	
	public String getStatus() {
		return status;
	}
	

}
