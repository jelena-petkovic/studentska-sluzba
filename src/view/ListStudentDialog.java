package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import model.BazaPredmeta;
import model.Predmet;
import model.Student;

public class ListStudentDialog extends JDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1467272794000992480L;

	public ListStudentDialog(Frame parent) {
		super(parent, "Lista studenata",true);
		
		DefaultListModel<String> model = new DefaultListModel<String>();
		JList<String> list = new JList<String>( model );

		int i = Entiteti.getInstance().getSelectedRowPredmet();
		Predmet p = BazaPredmeta.getInstance().getPredmet(i);
			
		for ( Student s : p.getSpisak_studenata_koji_slusaju() ){
		  System.out.println(s.getBroj_indeksa());
		  model.addElement( s.getBroj_indeksa() );
		}
		
		
		
		

		setSize(250, 250);
		setLocationRelativeTo(parent);
	
		
		JPanel panCenter = new JPanel();
		BoxLayout boxCenter = new BoxLayout(panCenter, BoxLayout.Y_AXIS);
		panCenter.setLayout(boxCenter);


		JScrollPane jScrollPanel = new JScrollPane();
		JPanel panel = new JPanel();
		panel.add(jScrollPanel, BorderLayout.CENTER);
		jScrollPanel.getViewport().add(list,null);

		
		

		panCenter.add(panel);


		panCenter.add(Box.createVerticalStrut(25));
		add(panCenter, BorderLayout.CENTER);		
		
		JPanel panBottom = new JPanel();
		BoxLayout box = new BoxLayout(panBottom, BoxLayout.X_AXIS);
		panBottom.setLayout(box);

		JButton btnOk = new JButton("Obrisi");
		btnOk.setPreferredSize(new Dimension(100, 25));
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(list.getSelectedIndex() == -1) {
					JOptionPane.showMessageDialog(parent, "Niste selektovali studenta!");
				}else {
					int i = list.getSelectedIndex();
					p.deleteStudent(i);
					setVisible(false);
				}
			}
		});
		

		JButton btnCancel = new JButton("nazad");
		btnCancel.setPreferredSize(new Dimension(100, 25));
		panBottom.add(Box.createGlue());
		panBottom.add(btnOk);
		panBottom.add(Box.createHorizontalStrut(10));
		panBottom.add(btnCancel);
		panBottom.add(Box.createHorizontalStrut(10));
		
		
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});

		add(panBottom, BorderLayout.SOUTH);
		pack();


	}
}
