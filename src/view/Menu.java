package view;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class Menu extends JMenuBar {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8806239592408194758L;

	public Menu(final JFrame parent) {
				
		JMenu file = new JMenu("File");
		JMenu help = new JMenu("Help");
		JMenu edit = new JMenu("Edit");
		
		file.setMnemonic(KeyEvent.VK_F);   //ALT + F
		help.setMnemonic(KeyEvent.VK_H);
		edit.setMnemonic(KeyEvent.VK_E);
		
		JMenuItem newFile = new JMenuItem("New");
		JMenuItem closeFile = new JMenuItem("Close");
		JMenuItem editEdit = new JMenuItem("Edit");
		JMenuItem deleteEdit = new JMenuItem("Delete");
		JMenuItem helpHelp = new JMenuItem("Help");
		JMenuItem aboutHelp = new JMenuItem("About");
		
		ImageIcon icon_create = new ImageIcon( new ImageIcon("src\\images\\add64.png").getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
		ImageIcon icon_edit =  new ImageIcon( new ImageIcon("src\\images\\pencil-edit2.png").getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
		ImageIcon icon_delete = new ImageIcon(new ImageIcon("src\\images\\delete64.png").getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
		ImageIcon icon_close = new ImageIcon(new ImageIcon("src\\images\\close.png").getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
		ImageIcon icon_about = new ImageIcon(new ImageIcon("src\\images\\about.png").getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
		ImageIcon icon_help = new ImageIcon(new ImageIcon("src\\images\\help.png").getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
		
		newFile.setIcon(icon_create);
		newFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK)); //CTRL + N
		closeFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK)); //CTRL + C
		closeFile.setIcon(icon_close);
		editEdit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK)); //CTRL + E
		editEdit.setIcon(icon_edit);
		deleteEdit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, ActionEvent.CTRL_MASK)); //CTRL + D
		deleteEdit.setIcon(icon_delete);
		helpHelp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.CTRL_MASK)); //CTRL + H
		helpHelp.setIcon(icon_help);
		aboutHelp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK)); //CTRL + A
		aboutHelp.setIcon(icon_about);
		
		newFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(Entiteti.getInstance().getSelectedIndex()==0) {	
					SimpleDialog dialog = new SimpleDialog(parent,"Dodavanje studenta",true);
					dialog.setVisible(true);
				}else if(Entiteti.getInstance().getSelectedIndex()==1) {
					AddDialogProfessor dialog = new AddDialogProfessor(parent,"Dodavanje profesora",true);
					dialog.setVisible(true);
				}else if(Entiteti.getInstance().getSelectedIndex()==2){
					AddDialogSubject dialog = new AddDialogSubject(parent,"Dodavanje predmeta",true);
					dialog.setVisible(true);
				}
			}
    	});
		
		deleteEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(Entiteti.getInstance().getSelectedIndex()==0) {	
					if(Entiteti.getInstance().getSelectedRowStudent() == -1) {
						JOptionPane.showMessageDialog(parent, "Niste selektovali studenta!");
					}else {
						DeleteDialog dialog = new DeleteDialog(parent);
						dialog.setVisible(true);
					}
				}else if(Entiteti.getInstance().getSelectedIndex()==1) {
					if(Entiteti.getInstance().getSelectedRowProfesor() == -1) {
						JOptionPane.showMessageDialog(parent, "Niste selektovali profesora!");
					}else {
						DeleteDialogProfesor dialog = new DeleteDialogProfesor(parent);
						dialog.setVisible(true);
					}
				}else if(Entiteti.getInstance().getSelectedIndex()==2){
					if(Entiteti.getInstance().getSelectedRowPredmet() == -1) {
						JOptionPane.showMessageDialog(parent, "Niste selektovali predmet!");
					}else {
						DeleteDialogPredmet dialog = new DeleteDialogPredmet(parent);
						dialog.setVisible(true);
					}
				}
			}
		});
		
		editEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(Entiteti.getInstance().getSelectedIndex()==0) {
					if(Entiteti.getInstance().getSelectedRowStudent() == -1) {
						JOptionPane.showMessageDialog(parent, "Niste selektovali studenta!");
					}else {
						EditDialog dialog = new EditDialog(parent);
						dialog.setVisible(true);
					}
				}else if(Entiteti.getInstance().getSelectedIndex()==1) {
					if(Entiteti.getInstance().getSelectedRowProfesor() == -1) {
						JOptionPane.showMessageDialog(parent, "Niste selektovali profesora!");
					}else {
						EditDialogProfesor dialog = new EditDialogProfesor(parent,"Izmena profesora",true);
						dialog.setVisible(true);
					}
				}else if(Entiteti.getInstance().getSelectedIndex()==1) {
					if(Entiteti.getInstance().getSelectedRowPredmet() == -1) {
						JOptionPane.showMessageDialog(parent, "Niste selektovali predmet!");
					}else {
						EditDialogPredmet dialog = new EditDialogPredmet(parent,"Izmena predmet",true);
						dialog.setVisible(true);
					}
				}
			}
		});
		
		closeFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		
		helpHelp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				DialogHelp dialog = new DialogHelp(parent,"O aplikaciji", true);
				dialog.setVisible(true);
			}
		});
		
		aboutHelp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				DialogAbout dialog = new DialogAbout(parent,"O autorima", true);
				dialog.setVisible(true);			}
		});
		
		file.add(newFile);
		file.addSeparator();
		file.add(closeFile);
		edit.add(editEdit);
		edit.addSeparator();
		edit.add(deleteEdit);
		help.add(helpHelp);
		help.addSeparator();
		help.add(aboutHelp);
		
		add(file);
		add(edit);
		add(help);
		
		
	}
}
