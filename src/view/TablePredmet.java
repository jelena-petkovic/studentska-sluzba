package view;

import model.BazaPredmeta;
import model.Predmet;

public class TablePredmet extends Table {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5812810445953639256L;
	private String[] columnNames = {"sifra_predmeta","naziv_predmeta","semestar","god_studija","predmetni_profesor","student"};
		

	public TablePredmet() {
		
	}

	@Override
	public void setValueAt(Object value, int row, int col) {
		System.out.println("Postavio vrednost " + value + " na " + row + ","
				+ col + " (tipa " + value.getClass() + ")");
		Predmet predmet = BazaPredmeta.getInstance().getPredmet(row);
		predmet.set(col, value);
		BazaPredmeta.getInstance().setPredmet(row, predmet);
		// prijavimo da smo promenili vrednost u tabeli
		// fireTableCellUpdated(row, col);
	}
	
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}
	@Override
	public int getRowCount() {
		return BazaPredmeta.getInstance().size();
	}
	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}
	@Override
	public Object getValueAt(int row, int col) {
		
		Predmet predmet = BazaPredmeta.getInstance().getPredmet(row);
		return predmet.toCell(col);
		
	}
	public Object getRow(int row) {
		Predmet predmet = BazaPredmeta.getInstance().getPredmet(row);
		return predmet;
	}

	/**
	 * Ako se ova metoda ne redefinise, koristi se default renderer/editor za
	 * celiju. To znaci da, ako je kolona tipa boolean, onda ce se u tabeli
	 * prikazati true/false, a ovako ce se za takav tip kolone pojaviti
	 * checkbox.
	 */

	@Override
	public boolean isCellEditable(int row, int col) {
		// Prva kolona ne moze da se menja
		if (col < 1) {
			return false;
		} else {
			return true;
		}
	}
}
