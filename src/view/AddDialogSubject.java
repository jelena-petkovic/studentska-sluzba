package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.PredmetController;
import model.Predmet;

public class AddDialogSubject extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1799894302168390265L;

	public AddDialogSubject(Frame parent, String title, boolean modal) {
		super(parent, title, modal);

		setSize(250, 250);
		setLocationRelativeTo(parent);
	
		
		JPanel panCenter = new JPanel();
		BoxLayout boxCenter = new BoxLayout(panCenter, BoxLayout.Y_AXIS);
		panCenter.setLayout(boxCenter);

		// dimenzije labela i tekstualnih komponenti
		Dimension dim = new Dimension(150, 20);

		JPanel panSifra = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblSifra = new JLabel("Sifra predmeta*");
		lblSifra.setPreferredSize(dim);
		JTextField txtSifra = new JTextField();
		txtSifra.setPreferredSize(dim);
		panSifra.add(lblSifra);
		panSifra.add(txtSifra);
		
		
		JPanel panNaziv = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblNaziv = new JLabel("Naziv predmeta*");
		lblNaziv.setPreferredSize(dim);
		JTextField txtNaziv = new JTextField();
		txtNaziv.setPreferredSize(dim);
		panNaziv.add(lblNaziv);
		panNaziv.add(txtNaziv);

		JPanel panSemestar = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblSemestar = new JLabel("Semestar*");
		lblSemestar.setPreferredSize(dim);
		JTextField txtSemestar = new JTextField();
		txtSemestar.setPreferredSize(dim);
		panSemestar.add(lblSemestar);
		panSemestar.add(txtSemestar);
		
		JPanel panGodStudija = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblGodStudija = new JLabel("Godina studija*");
		lblGodStudija.setPreferredSize(dim);
		JTextField txtGodStudija = new JTextField();
		txtGodStudija.setPreferredSize(dim);
		panGodStudija.add(lblGodStudija);
		panGodStudija.add(txtGodStudija);
				
		panCenter.add(Box.createGlue());
		panCenter.add(panSifra);
		panCenter.add(panNaziv);
		panCenter.add(panSemestar);
		panCenter.add(panGodStudija);
		panCenter.add(Box.createVerticalStrut(25));
		add(panCenter, BorderLayout.CENTER);		
		
		JPanel panBottom = new JPanel();
		BoxLayout box = new BoxLayout(panBottom, BoxLayout.X_AXIS);
		panBottom.setLayout(box);

		JButton btnOk = new JButton("Potvrda");
		btnOk.setPreferredSize(new Dimension(100, 25));
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {		 
				
				if(txtSifra.getText().isEmpty() || txtNaziv.getText().isEmpty()||txtSemestar.getText().isEmpty()
						|| txtGodStudija.getText().isEmpty()) {
					JOptionPane.showMessageDialog(parent, "Morate popuniti sva polja!");
				}else {
				
				Predmet p = new Predmet(txtSifra.getText(),txtNaziv.getText(),txtSemestar.getText(), txtGodStudija.getText());

				PredmetController.getInstance().dodajPredmet(p);
				setVisible(false);
				}
			}
		});
		

		JButton btnCancel = new JButton("Odustanak");
		btnCancel.setPreferredSize(new Dimension(100, 25));
		panBottom.add(Box.createGlue());
		panBottom.add(btnOk);
		panBottom.add(Box.createHorizontalStrut(10));
		panBottom.add(btnCancel);
		panBottom.add(Box.createHorizontalStrut(10));
		
		
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});

		add(panBottom, BorderLayout.SOUTH);
		pack();


	}
}
