package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.PredmetController;
import model.BazaPredmeta;
import model.Predmet;

public class EditDialogPredmet extends JDialog{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9033040103518408876L;

	public EditDialogPredmet(Frame parent, String title, boolean modal) {
		super(parent, title, modal);

		setSize(250, 250);
		setLocationRelativeTo(parent);
	
		int row = Entiteti.getInstance().getSelectedRowPredmet();
		Predmet p = BazaPredmeta.getInstance().getPredmet(row);
		
		
		JPanel panCenter = new JPanel();
		BoxLayout boxCenter = new BoxLayout(panCenter, BoxLayout.Y_AXIS);
		panCenter.setLayout(boxCenter);

		// dimenzije labela i tekstualnih komponenti
		Dimension dim = new Dimension(150, 20);

		JPanel panSifra = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblSifra = new JLabel("Sifra predmeta*");
		lblSifra.setPreferredSize(dim);
		JTextField txtSifra = new JTextField();
		txtSifra.setPreferredSize(dim);
		txtSifra.setText(p.getSifra_predmeta());
		panSifra.add(lblSifra);
		panSifra.add(txtSifra);
		
		
		JPanel panNaziv = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblNaziv = new JLabel("Naziv predmeta*");
		lblNaziv.setPreferredSize(dim);
		JTextField txtNaziv = new JTextField();
		txtNaziv.setText(p.getNaziv_predmeta());
		txtNaziv.setPreferredSize(dim);
		panNaziv.add(lblNaziv);
		panNaziv.add(txtNaziv);

		JPanel panSemestar = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblSemestar = new JLabel("Semestar*");
		lblSemestar.setPreferredSize(dim);
		JTextField txtSemestar = new JTextField();
		txtSemestar.setText(p.getSemestar());
		txtSemestar.setPreferredSize(dim);
		panSemestar.add(lblSemestar);
		panSemestar.add(txtSemestar);
		
		JPanel panGodStudija = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblGodStudija = new JLabel("Godina studija*");
		lblGodStudija.setPreferredSize(dim);
		JTextField txtGodStudija = new JTextField();
		txtGodStudija.setPreferredSize(dim);
		txtGodStudija.setText(p.getGod_studija());
		panGodStudija.add(lblGodStudija);
		panGodStudija.add(txtGodStudija);
				
		panCenter.add(Box.createGlue());
		panCenter.add(panSifra);
		panCenter.add(panNaziv);
		panCenter.add(panSemestar);
		panCenter.add(panGodStudija);
		panCenter.add(Box.createVerticalStrut(25));
		add(panCenter, BorderLayout.CENTER);		
		
		JPanel panBottom = new JPanel();
		BoxLayout box = new BoxLayout(panBottom, BoxLayout.X_AXIS);
		panBottom.setLayout(box);

		JButton btnOk = new JButton("Potvrda");
		btnOk.setPreferredSize(new Dimension(100, 25));
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {		 
				 
				Predmet p = new Predmet(txtSifra.getText(),txtNaziv.getText(),txtSemestar.getText(), txtGodStudija.getText());

				PredmetController.getInstance().izmeniPredmet(row, p);;
				setVisible(false);
				
			}
		});
		

		JButton btnCancel = new JButton("Odustanak");
		btnCancel.setPreferredSize(new Dimension(100, 25));
		panBottom.add(Box.createGlue());
		panBottom.add(btnOk);
		panBottom.add(Box.createHorizontalStrut(10));
		panBottom.add(btnCancel);
		panBottom.add(Box.createHorizontalStrut(10));
		
		
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});

		add(panBottom, BorderLayout.SOUTH);
		pack();


	}
}
