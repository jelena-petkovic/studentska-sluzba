package view;

import java.awt.event.KeyEvent;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;






public class Entiteti extends JTabbedPane {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1142047409491323469L;

	static boolean isPressed = false;

	private JTable tableStudent;
	private JTable tablePredmet;
	private JTable tableProfesor;
	
	private static Entiteti instance = null;
	
	public static Entiteti getInstance(final JFrame parent) {
		if (instance == null) {
			instance = new Entiteti(parent);
		}
		return instance;
		
	}
	
	public static Entiteti getInstance() {
	
		return instance;
		
	}
	
	private Entiteti(final JFrame parent){
				  
		  //komentar
		
	
		
		
		  tableStudent = new JTable(new TableStudent());
		  tableStudent.setAutoCreateRowSorter(true);
		  
		  tableProfesor = new JTable(new TableProfesor());
		  tableProfesor.setAutoCreateRowSorter(true);
		  
		  tablePredmet = new JTable(new TablePredmet());
		  tablePredmet.setAutoCreateRowSorter(true);
			 
		  tablePredmet.addMouseListener(new java.awt.event.MouseAdapter() {
			  @Override
			   public void mouseClicked(java.awt.event.MouseEvent evt) {
			      int row = tablePredmet.rowAtPoint(evt.getPoint());
			      int col = tablePredmet.columnAtPoint(evt.getPoint());
			      if (row >= 0 && col == 5) {
			    	  
			    	  ListStudentDialog dialog = new ListStudentDialog(parent);
			    	  dialog.setVisible(true);
			    	  
			      }
			   }
			  });
		  
		  JScrollPane jScrollPanel1 = new JScrollPane();
		  jScrollPanel1.getViewport().add(tableStudent,null);
		  this.addTab("STUDENT",jScrollPanel1);
		  this.setMnemonicAt(0, KeyEvent.VK_1);
	

		  JScrollPane jScrollPanel2 = new JScrollPane();
		  jScrollPanel2.getViewport().add(tableProfesor,null);
		  this.addTab("PROFESOR",jScrollPanel2);
		  this.setMnemonicAt(1, KeyEvent.VK_2);

		  
		  JScrollPane jScrollPanel3 = new JScrollPane();
		  jScrollPanel3.getViewport().add(tablePredmet,null);
		  this.addTab("PREDMET", jScrollPanel3);
		  this.setMnemonicAt(2, KeyEvent.VK_3);

		}

		@SuppressWarnings("unused")
		private JComponent makeTextPanel(String text, boolean f) {
			JPanel panel = new JPanel(false);
			JLabel filler = new JLabel(text);
			panel.add(filler);
			
			return panel;
		}
		

		
		public void azurirajPrikaz() {
			TableStudent model = (TableStudent) tableStudent.getModel();
			
			model.fireTableDataChanged();
			validate();
		}
		
		public void azurirajPrikazProfesor() {
			TableProfesor model = (TableProfesor) tableProfesor.getModel();
			model.fireTableDataChanged();
			validate();
		}
		public void azurirajPrikazPredmet() {
			TablePredmet model = (TablePredmet) tablePredmet.getModel();
			model.fireTableDataChanged();
			validate();
		}
		
		public int getSelectedRowStudent() {
			return tableStudent.getSelectedRow();
		}
		
		public int getSelectedRowPredmet() {
			return tablePredmet.getSelectedRow();
		}
		
		public int getSelectedRowProfesor() {
			return tableProfesor.getSelectedRow();
		}
		
		public JTable getStudentTable() {
			return tableStudent;
		}
}

