package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.BazaPredmeta;
import model.BazaProfesora;
import model.BazaStudenata;
import model.Predmet;
import model.Profesor;
import model.Status;
import model.Student;


public class ToolBar extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4480175195628913435L;
	JButton addProfesor;
	JButton addStudent;

	public ToolBar(final JFrame parent) {
		//super(SwingConstants.HORIZONTAL);
		this.setLayout(new BoxLayout(this,BoxLayout.X_AXIS));
		
		ImageIcon icon_create = new ImageIcon( new ImageIcon("src\\images\\add64.png").getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		ImageIcon icon_edit =  new ImageIcon( new ImageIcon("src\\images\\pencil-edit2.png").getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		ImageIcon icon_delete = new ImageIcon(new ImageIcon("src\\images\\delete64.png").getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		ImageIcon icon_student = new ImageIcon(new ImageIcon("src\\images\\student64.png").getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		ImageIcon icon_profesor = new ImageIcon(new ImageIcon("src\\images\\professor64.png").getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		ImageIcon icon_search = new ImageIcon(new ImageIcon("src\\images\\search64.png").getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		
		JButton create = new JButton(icon_create);
		create.setToolTipText("Create");
		create.setBackground(Color.white);
		create.setPreferredSize(new Dimension(60, 50));
		JButton edit = new JButton(icon_edit);
		edit.setToolTipText("Edit");
		edit.setBackground(Color.white);
		edit.setPreferredSize(new Dimension(60, 50));
		JButton delete = new JButton(icon_delete);
		delete.setPreferredSize(new Dimension(60, 50));
		delete.setToolTipText("Delete");
		delete.setBackground(Color.white);
		addStudent = new JButton(icon_student);
		addStudent.setPreferredSize(new Dimension(60, 50));
		addStudent.setToolTipText("AddStudent");
		addStudent.setBackground(Color.white);
		addProfesor = new JButton(icon_profesor);
		addProfesor.setPreferredSize(new Dimension(60, 50));
		addProfesor.setToolTipText("AddProfesor");
		addProfesor.setBackground(Color.white);
		addStudent.setToolTipText("addStudent");
		
		JTextField search = new JTextField();
		search.setPreferredSize(new Dimension(150,40));
		search.setMaximumSize( search.getPreferredSize());
		JButton search_button = new JButton(icon_search);
		search_button.setBackground(Color.white);
		

		search_button.addActionListener(new ActionListener() {
			
		
			
			
			
			
			
			public void actionPerformed(ActionEvent arg0) {
				
				if(Entiteti.getInstance().getSelectedIndex()==0) {	
					
					String ime = null;
					String prezime = null;
					LocalDate date = null;
					String adresa = null;
					String broj = null;
					String indeks = null;
					Integer godinaStudija = null;
					Status status = null;
					
										
					
					String[] fields = search.getText().split(";");
					for ( String s : fields ) {
						String[] pair = s.split(":");
						switch(pair[0]) {
						  case "ime":
							  ime = pair[1];
						    break;
						  case "prezime":
							  prezime = pair[1];
						    break;
						  case "indeks":
							  indeks = pair[1];
							break;
						  case "status":
							  if ( pair[1] == "B")
								  status=Status.B;
							  else 
								  status=Status.S;
							break;
						  case "godinastudija":
							  godinaStudija = Integer.parseInt(pair[1]);
							break;
						  case "broj":
							  broj = pair[1];
							break;

							  
						}
					}

					
					BazaStudenata.getInstance().refresh();
					
					for ( Student s : BazaStudenata.getInstance().getTemp() ) {
						System.out.println();
						if ( ime != null && !ime.equals(s.getIme()) ) {
							continue;
						} else if ( prezime != null && !prezime.equals(s.getPrezime()) ) {
							continue;
						} else if ( indeks != null && !indeks.equals(s.getBroj_indeksa()) ) {
							continue;
						} else if ( godinaStudija != null && godinaStudija != s.getTrenutna_godina_studija() ) {
							continue;
						} else if ( status != null && !(status == s.getStatus()) ) {
							continue;
						}else {
							BazaStudenata.getInstance().suitable(s);
						}
					}
					Entiteti.getInstance().azurirajPrikaz();
					
					
				}else if(Entiteti.getInstance().getSelectedIndex()==1) {
					
					String ime = null;
					String prezime = null;
					LocalDate date = null;
					String adresa = null;
					String brL = null;
					Status status = null;
					
					
					
					
					String[] fields = search.getText().split(";");
					for ( String s : fields ) {
						String[] pair = s.split(":");
						switch(pair[0]) {
						  case "ime":
							  ime = pair[1];
						    break;
						  case "prezime":
							  prezime = pair[1];
						    break;
						  case "brL":
							  brL = pair[1];
							break;
							  
						}
					}
					
					BazaProfesora.getInstance().refresh();
					
					for ( Profesor s : BazaProfesora.getInstance().getTemp() ) {
						System.out.println();
						if ( ime != null && !ime.equals(s.getIme()) ) {
							continue;
						} else if ( prezime != null && !prezime.equals(s.getPrezime()) ) {
							continue;
						} else if ( brL != null && !brL.equals(s.getBr_licne_karte()) ) {
							continue;
						} else {
							BazaProfesora.getInstance().suitable(s);
						}
					}
					Entiteti.getInstance().azurirajPrikazProfesor();
					
					
				}else if(Entiteti.getInstance().getSelectedIndex()==2){
					
					String sifrapredmeta=null;
					String nazivpredmeta=null;
					String semestar=null; 			
					String godstudija=null;
					
					
					String[] fields = search.getText().split(";");
					for ( String s : fields ) {
						String[] pair = s.split(":");
						switch(pair[0]) {
						  case "sifrapredmeta":
							  sifrapredmeta = pair[1];
						    break;
						  case "nazivpredmeta":
							  nazivpredmeta = pair[1];
						    break;
						  case "godstudija":
							  godstudija = pair[1];
							break;
							  
						}
					}
					
					BazaPredmeta.getInstance().refresh();
					
					for ( Predmet p : BazaPredmeta.getInstance().getTemp() ) {
						System.out.println();
						if ( sifrapredmeta != null && !sifrapredmeta.equals(p.getSifra_predmeta()) ) {
							continue;
						} else if ( nazivpredmeta != null && !nazivpredmeta.equals(p.getNaziv_predmeta()) ) {
							continue;
						} else if ( godstudija != null && !godstudija.equals(p.getGod_studija()) ) {
							continue;
						} else {
							BazaPredmeta.getInstance().suitable(p);
						}
					}
					Entiteti.getInstance().azurirajPrikazPredmet();
					
				}
				
				

				
				
			}
		});
		
				
		
		search_button.setPreferredSize(new Dimension(40, 40));
		
		search_button.setMaximumSize( search_button.getPreferredSize());
		create.setToolTipText("search");
		addStudent.setVisible(false);
		addProfesor.setVisible(false);

		add(create);
		add(edit);
		add(delete);
		add(addStudent);
		add(addProfesor);		
		add(Box.createGlue());
		
		add(search);
		add(Box.createHorizontalStrut(5)); 
		add(search_button);
		
		setBackground(Color.LIGHT_GRAY);
		
		create.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					if(Entiteti.getInstance().getSelectedIndex()==0) {	
						SimpleDialog dialog = new SimpleDialog(parent,"Dodavanje studenta",true);
						dialog.setVisible(true);
					}else if(Entiteti.getInstance().getSelectedIndex()==1) {
						AddDialogProfessor dialog = new AddDialogProfessor(parent,"Dodavanje profesora",true);
						dialog.setVisible(true);
					}else if(Entiteti.getInstance().getSelectedIndex()==2){
						AddDialogSubject dialog = new AddDialogSubject(parent,"Dodavanje predmeta",true);
						dialog.setVisible(true);
					}
				}
		});
		
		edit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(Entiteti.getInstance().getSelectedIndex()==0) {
					if(Entiteti.getInstance().getSelectedRowStudent() == -1) {
						JOptionPane.showMessageDialog(parent, "Greska!Morate oznaciti studenta!");
					}else {
						EditDialog dialog = new EditDialog(parent);
						dialog.setVisible(true);
					}
				}else if(Entiteti.getInstance().getSelectedIndex()==1) {
					if(Entiteti.getInstance().getSelectedRowProfesor() == -1) {
						JOptionPane.showMessageDialog(parent, "Greska!Morate oznaciti profesora!");
					}else {
						EditDialogProfesor dialog = new EditDialogProfesor(parent,"Izmena profesora",true);
						dialog.setVisible(true);
					}
				}else if(Entiteti.getInstance().getSelectedIndex()==2) {
					if(Entiteti.getInstance().getSelectedRowPredmet() == -1) {
						JOptionPane.showMessageDialog(parent, "Greska!Morate oznaciti predmet!");
					}else {
						EditDialogPredmet dialog = new EditDialogPredmet(parent,"Izmena predmet",true);
						dialog.setVisible(true);
					}
				}
			}
		});
		delete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(Entiteti.getInstance().getSelectedIndex()==0) {	
					if(Entiteti.getInstance().getSelectedRowStudent() == -1) {
						JOptionPane.showMessageDialog(parent, "Greska!Morate oznaciti studenta!");
					}else {
						DeleteDialog dialog = new DeleteDialog(parent);
						dialog.setVisible(true);
					}
				}else if(Entiteti.getInstance().getSelectedIndex()==1) {
					if(Entiteti.getInstance().getSelectedRowProfesor() == -1) {
						JOptionPane.showMessageDialog(parent, "Greska!Morate oznaciti profesora!");
					}else {
						DeleteDialogProfesor dialog = new DeleteDialogProfesor(parent);
						dialog.setVisible(true);
					}
				}else if(Entiteti.getInstance().getSelectedIndex()==2){
					if(Entiteti.getInstance().getSelectedRowPredmet() == -1) {
						JOptionPane.showMessageDialog(parent, "Greska!Morate oznaciti predmet!");
					}else {
						DeleteDialogPredmet dialog = new DeleteDialogPredmet(parent);
						dialog.setVisible(true);
					}
				}
			}
		});
		
		addStudent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(Entiteti.getInstance().getSelectedRowPredmet() == -1) {
					JOptionPane.showMessageDialog(parent, "Greska!Morate oznaciti predmet!");
				}else {
					AddStudentDialog dialog = new AddStudentDialog(parent);
					dialog.setVisible(true);
				}
			}
		});
		
		
		
		
		
		addProfesor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(Entiteti.getInstance().getSelectedRowPredmet() == -1) {
					JOptionPane.showMessageDialog(parent, "Greska!Morate oznaciti predmet!");
				}else {
					AddProfesorDialog dialog = new AddProfesorDialog(parent);
					dialog.setVisible(true);
				}
			}
		});
		
		
	}
		
	public void setVisibleButton(boolean value) {
		if(value) {
			addStudent.setVisible(true);
			addProfesor.setVisible(true);
		}else {
			addStudent.setVisible(false);
			addProfesor.setVisible(false);
		}
	}
	
}
