package view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Image;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class DialogAbout extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7874835564355069612L;
	
	public DialogAbout(Frame parent, String title, boolean modal) {
		super(parent, title, modal);

		setSize(500, 500);
		setLocationRelativeTo(parent);
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

		JTextArea jta = new JTextArea(5, 20);
		String text = "  Aplikacija je namenjena za rukovanje studentskom sluzbom."
				+ "\nMoze se rukovati entitetima: predmet, profesor i student."
				+ "\nTakodje i njihovim medjusobnim vezama\n\n"
				+ "O autorima:\n"
				+ "Jelena Petkovic RA127/2017.\n"
				+ "Stefan Zindovic RA1**/2017"; //izmeni
				
        jta.setText(text);
        jta.setEditable(false);
        add(jta);
		ImageIcon image1 = new ImageIcon( new ImageIcon("src\\images\\jelena.jpg").getImage().getScaledInstance(300, 300, Image.SCALE_DEFAULT));
		ImageIcon image2 = new ImageIcon( new ImageIcon("src\\images\\stefan.jpg").getImage().getScaledInstance(300, 300, Image.SCALE_DEFAULT));

		JPanel pan = new JPanel();
	    pan.setLayout(new FlowLayout());
		JLabel label1 = new JLabel(image1);
	    JLabel label2 = new JLabel(image2);
	    label1.setPreferredSize(new Dimension(200,300));
	    pan.add(label1);
	    label2.setPreferredSize(new Dimension(200,300));
	    pan.add(label2);
        add(pan);
	}

}
