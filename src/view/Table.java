package view;


import javax.swing.table.AbstractTableModel;

/**
 * Reprezentuje model podataka u tabeli.
 */
abstract class Table extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3525597330822287160L;

	/*
	
	private String[] columnNames;
	
	private List<Student> data;
	
	/
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}
	@Override
	public int getRowCount() {
		return data.size();
	}
	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}
	@Override
	public Object getValueAt(int row, int col) {
		Student student = data.get(row);
		return student.toCell(col);
	}

	@Override
	/**
	 * Ako se ova metoda ne redefinise, koristi se default renderer/editor za
	 * celiju. To znaci da, ako je kolona tipa boolean, onda ce se u tabeli
	 * prikazati true/false, a ovako ce se za takav tip kolone pojaviti
	 * checkbox.
	 *//*
	public Class<?> getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}
	@Override
	public boolean isCellEditable(int row, int col) {
		// Prva kolona ne moze da se menja
		if (col < 1) {
			return false;
		} else {
			return true;
		}
	}
	@Override
	
	abstract public void setValueAt(Object value, int row, int col);
	
	public void deleteRow(int row) {
		data.remove(row);
		fireTableDataChanged();
	}
	
	
	*/
}
