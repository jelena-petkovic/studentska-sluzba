package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import controller.ProfesorController;
import model.BazaProfesora;
import model.Profesor;
import model.Titula;

public class EditDialogProfesor extends JDialog{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2890193629254249206L;

	public EditDialogProfesor(Frame parent, String title, boolean modal) {
		super(parent, title, modal);
		
		int row = Entiteti.getInstance().getSelectedRowProfesor();
		Profesor p = BazaProfesora.getInstance().getProfesor(row);
		
		setSize(250, 250);
		setLocationRelativeTo(parent);
	
		
		JPanel panCenter = new JPanel();
		BoxLayout boxCenter = new BoxLayout(panCenter, BoxLayout.Y_AXIS);
		panCenter.setLayout(boxCenter);

		// dimenzije labela i tekstualnih komponenti
		Dimension dim = new Dimension(150, 20);

		JPanel panIme = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblIme = new JLabel("Ime*");
		lblIme.setPreferredSize(dim);
		JTextField txtIme = new JTextField();
		txtIme.setText(p.getIme());
		txtIme.setPreferredSize(dim);
		panIme.add(lblIme);
		panIme.add(txtIme);
		
		
		JPanel panPrezime = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblPrezime = new JLabel("Prezime*");
		lblPrezime.setPreferredSize(dim);
		JTextField txtPrezime = new JTextField();
		txtPrezime.setPreferredSize(dim);
		txtPrezime.setText(p.getPrezime());
		panPrezime.add(lblPrezime);
		panPrezime.add(txtPrezime);

		JPanel panDatum = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblDatum = new JLabel("Datum rodjenja*");
		lblDatum.setPreferredSize(dim);
		JTextField txtDatum = new JTextField();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
		txtDatum.setText(p.getDatum_rodjenja().format(formatter));
		txtDatum.setPreferredSize(dim);
		panDatum.add(lblDatum);
		panDatum.add(txtDatum);
		
		JPanel panAdresa = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblAdresa = new JLabel("Adresa stanovanja*");
		lblAdresa.setPreferredSize(dim);
		JTextField txtAdresa = new JTextField();
		txtAdresa.setPreferredSize(dim);
		txtAdresa.setText(p.getAdresa_stanovanja());
		panAdresa.add(lblAdresa);
		panAdresa.add(txtAdresa);
		
		JPanel panBroj = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblBroj = new JLabel("Broj telefona*");
		lblBroj.setPreferredSize(dim);
		JTextField txtBroj = new JTextField();
		txtBroj.setPreferredSize(dim);
		txtBroj.setText(p.getKontakt_telefon());
		panBroj.add(lblBroj);
		panBroj.add(txtBroj);
	
		JPanel panLicnaKarta = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblLicnaKarta = new JLabel("Broj licne karte*");
		lblLicnaKarta.setPreferredSize(dim);
		JTextField txtLicnaKarta = new JTextField();
		txtLicnaKarta.setPreferredSize(dim);
		txtLicnaKarta.setText(p.getBr_licne_karte());
		panLicnaKarta.add(lblLicnaKarta);
		panLicnaKarta.add(txtLicnaKarta);
		
		JPanel panAdresaKancelarije = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblAdresaKancelarije = new JLabel("Adresa kancelarije*");
		lblAdresaKancelarije.setPreferredSize(dim);
		JTextField txtAdresaKancelarije = new JTextField();
		txtAdresaKancelarije.setPreferredSize(dim);
		panAdresaKancelarije.add(lblAdresaKancelarije);
		panAdresaKancelarije.add(txtAdresaKancelarije);
		
		JPanel panZvanje = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblZvanje = new JLabel("Zvanje*");
		lblZvanje.setPreferredSize(dim);
		JTextField txtZvanje = new JTextField();
		txtZvanje.setPreferredSize(dim);
		txtZvanje.setText(p.getZvanje());
		panZvanje.add(lblZvanje);
		panZvanje.add(txtZvanje);
		

		JRadioButton titula1 = new JRadioButton("Titula1");
		JRadioButton titula2 = new JRadioButton("Titula2");
		ButtonGroup btnGroup1 = new ButtonGroup();
		btnGroup1.add(titula1);
		btnGroup1.add(titula2);
		
		if ( p.getTitula() == Titula.T1 ) {
			titula1.setSelected(true);
		} else {
			titula2.setSelected(true);
		}
		JPanel panTitula1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panTitula1.add(titula1);
		
		JPanel panTitula2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panTitula2.add(titula2);

		
		panCenter.add(Box.createGlue());
		panCenter.add(panPrezime);
		panCenter.add(panIme);
		panCenter.add(panDatum);
		panCenter.add(panAdresa);
		panCenter.add(panBroj);
		panCenter.add(panAdresaKancelarije);
		panCenter.add(panLicnaKarta);
		panCenter.add(panTitula1);
		panCenter.add(panTitula2);
		panCenter.add(panZvanje);

		panCenter.add(Box.createVerticalStrut(25));
		add(panCenter, BorderLayout.CENTER);		
		
		JPanel panBottom = new JPanel();
		BoxLayout box = new BoxLayout(panBottom, BoxLayout.X_AXIS);
		panBottom.setLayout(box);

		JButton btnOk = new JButton("Potvrda");
		btnOk.setPreferredSize(new Dimension(100, 25));
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				Titula titula;
				
				 if (titula1.isSelected()) {
		                titula=Titula.T1;
		        } else {
		        		titula=Titula.T2;
		        }
				 
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
				 
				Profesor p = new Profesor(txtIme.getText(),txtPrezime.getText(),LocalDate.parse(txtDatum.getText(), formatter),
						txtAdresa.getText(), txtBroj.getText(),txtAdresaKancelarije.getText(), txtLicnaKarta.getText(), titula, txtZvanje.getText());

				ProfesorController.getInstance().izmeniProfesora(row, p);
				setVisible(false);
				
			}
		});
		

		JButton btnCancel = new JButton("Odustanak");
		btnCancel.setPreferredSize(new Dimension(100, 25));
		panBottom.add(Box.createGlue());
		panBottom.add(btnOk);
		panBottom.add(Box.createHorizontalStrut(10));
		panBottom.add(btnCancel);
		panBottom.add(Box.createHorizontalStrut(10));
		
		
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});

		add(panBottom, BorderLayout.SOUTH);
		pack();


	}
}
