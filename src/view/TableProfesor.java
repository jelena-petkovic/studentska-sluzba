package view;

import model.BazaProfesora;
import model.Profesor;

public class TableProfesor extends Table {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6002662886082491598L;
	private String[] columnNames = {"Ime", "Prezime","Godina rodjenja","Adresa","telefon","email","adresa_kancelarije","br_licne_karte","titula","zvanje"};
	

	public TableProfesor() {
		
    }

	@Override
	public void setValueAt(Object value, int row, int col) {
		System.out.println("Postavio vrednost " + value + " na " + row + ","
				+ col + " (tipa " + value.getClass() + ")");
		Profesor profesor = BazaProfesora.getInstance().getProfesor(row);
		profesor.set(col, value);
		BazaProfesora.getInstance().setProfesor(row, profesor);
		// prijavimo da smo promenili vrednost u tabeli
		// fireTableCellUpdated(row, col);
	}
	
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}
	@Override
	public int getRowCount() {
		return BazaProfesora.getInstance().size();
	}
	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}
	@Override
	public Object getValueAt(int row, int col) {
		Profesor profesor = BazaProfesora.getInstance().getProfesor(row);
		return profesor.toCell(col);
	}
	public Object getRow(int row) {
		Profesor profesor = BazaProfesora.getInstance().getProfesor(row);
		return profesor;
	}

	@Override
	/**
	 * Ako se ova metoda ne redefinise, koristi se default renderer/editor za
	 * celiju. To znaci da, ako je kolona tipa boolean, onda ce se u tabeli
	 * prikazati true/false, a ovako ce se za takav tip kolone pojaviti
	 * checkbox.
	 */
	public Class<?> getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}
	@Override
	public boolean isCellEditable(int row, int col) {
		// Prva kolona ne moze da se menja
		if (col < 1) {
			return false;
		} else {
			return true;
		}
	}
}
