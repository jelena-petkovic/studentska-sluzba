package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.PredmetController;
import model.BazaPredmeta;
import model.BazaProfesora;
import model.Predmet;
import model.Profesor;

public class AddProfesorDialog extends JDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5831278685110622881L;

	public AddProfesorDialog(Frame parent) {
		
		super(parent, "Predmet - dodavanje profesora",true);

		setSize(250, 250);
		setLocationRelativeTo(parent);
	
		
		JPanel panCenter = new JPanel();
		BoxLayout boxCenter = new BoxLayout(panCenter, BoxLayout.Y_AXIS);
		panCenter.setLayout(boxCenter);

		// dimenzije labela i tekstualnih komponenti
		Dimension dim = new Dimension(150, 20);

	
		JPanel panBrL = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblBrL = new JLabel("Broj licne karte profesora");
		lblBrL.setPreferredSize(dim);
		JTextField txtBrL = new JTextField();
		txtBrL.setPreferredSize(dim);
		panBrL.add(lblBrL);
		panBrL.add(txtBrL);

		
		

		panCenter.add(panBrL);


		panCenter.add(Box.createVerticalStrut(25));
		add(panCenter, BorderLayout.CENTER);		
		
		JPanel panBottom = new JPanel();
		BoxLayout box = new BoxLayout(panBottom, BoxLayout.X_AXIS);
		panBottom.setLayout(box);

		JButton btnOk = new JButton("Potvrda");
		btnOk.setPreferredSize(new Dimension(100, 25));
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				int i = Entiteti.getInstance().getSelectedRowPredmet();
				Predmet p = BazaPredmeta.getInstance().getPredmet(i);
				
				String brLProfesora = txtBrL.getText();
				Profesor prof = BazaProfesora.getInstance().getProfesor(brLProfesora);
				
				
				
				if ( prof != null ) {
					PredmetController.getInstance().dodajProfesora(prof, p);
					setVisible(false);
				} else {
					btnOk.setEnabled(false);
				}
				
				
				
			}
		});
		

		JButton btnCancel = new JButton("Odustanak");
		btnCancel.setPreferredSize(new Dimension(100, 25));
		panBottom.add(Box.createGlue());
		panBottom.add(btnOk);
		panBottom.add(Box.createHorizontalStrut(10));
		panBottom.add(btnCancel);
		panBottom.add(Box.createHorizontalStrut(10));
		
		
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});

		add(panBottom, BorderLayout.SOUTH);
		pack();


	}
}
