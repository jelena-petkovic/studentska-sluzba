package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension; import java.awt.Image; import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MainFrame extends JFrame { 

	/**
	 * 
	 */
	private static final long serialVersionUID = -970588349232020428L;

	public MainFrame() {     
	  Toolkit kit = Toolkit.getDefaultToolkit();             
	  Dimension screenSize = kit.getScreenSize();                  
	  int screenHeight = screenSize.height;                
	  int screenWidth = screenSize.width;             // Podesavamo dimenzije prozora na polovinu dimenzija ekrana
	  setSize(screenWidth * 3 / 4, screenHeight * 3 / 4);               // Dodeljujemo ikonu 
	  Image img = kit.getImage("images/iko.jpg");               
	  setIconImage(img);                      //Podesavamo naslov              
	  setTitle("Studentska sluzba");              //Postavljamo akciju pri zatvaranju prozora              
	  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	  setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		this.addWindowListener(new WindowAdapter() {  //pitanje da li zelimo da izadjemo
			@Override
			public void windowClosing(WindowEvent e) {
				int answer = JOptionPane
						.showConfirmDialog(MainFrame.this,
								"Da li zelite da prekinete sa radom?",
								"Kraj rada", JOptionPane.YES_NO_OPTION,
								JOptionPane.QUESTION_MESSAGE);
				if (answer == JOptionPane.YES_OPTION)
					System.exit(0);
			}
		}); // addWindowListener
	  

	  setLocationRelativeTo(null);
	  getContentPane().setBackground(Color.WHITE);
	  Menu menu = new Menu(this);
	  menu.setVisible(true);
	  this.setJMenuBar(menu);
	  
	  ToolBar toolbar = new ToolBar(this);
	  this.add(toolbar,BorderLayout.NORTH);
	  
	  StatusBar statusbar = new StatusBar();
	  statusbar.setPreferredSize(new Dimension(800,40));
	  this.add(statusbar,BorderLayout.SOUTH);
	
	  Entiteti en = Entiteti.getInstance(this);
	  this.add(en,BorderLayout.CENTER);
	  en.addChangeListener(new ChangeListener() {
		    public void stateChanged(ChangeEvent e) {
		        if(en.getSelectedIndex() ==2) {
		        	toolbar.setVisibleButton(true);
		        }else {
		        	toolbar.setVisibleButton(false);
		        }
		    }
		});
  } 
  
 }
 