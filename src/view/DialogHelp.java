package view;

import java.awt.BorderLayout;
import java.awt.Frame;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class DialogHelp extends JDialog {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8412086968595887278L;

	public DialogHelp(Frame parent, String title, boolean modal) {
		super(parent, title, modal);
		this.setLayout(new BorderLayout());
		setSize(500, 500);
		setLocationRelativeTo(parent);
		JTextArea jta = new JTextArea(5, 20);
		String text = "Pritirskom na dugme new dodaje se entitet u onu tabelu koju je\n"
				+ "korisnik selektovao. Delete brise, a izmeni menja entitet.\n"
				+ "Moze se i searchovati odredjeni entitet.\n"
				+ "\nPostoji i opcija u toolbaru za dodavanje\n"
				+ "studenta i profesora na predmet, a ako zelite da\n"
				+ "ih obrisete postoji polje u tabeli \"prikazi\" \n"
				+ "koje prikazuje sve profesor ili studente na tom predmetu \n"
				+ "i dodatno dugme za brisanje profesora ili studenta \n"
				+ "sa tog predmeta.\n\n"
				+ "Mogu se koristiti sledece precice radi brzeg koriscenja aplikacije:\n"
				+ "Alt+f  ---   File\n"
				+ "\tCtrl+n  ---   New\n"
				+ "\tCtrl+c  ---   Close\n"
				+ "Alt+e  ---   Edit\n"
				+ "\tCtrl+e  ---   Edit\n"
				+ "\tCtrl+d  ---   Delete\n"
				+ "Alt+h  ---   Help\n"
				+ "\tCtrl+h  ---   Help\n"
				+ "\tCtrl+a  ---   About\n";
				
        jta.setText(text);
        jta.setEditable(false);
        JScrollPane jsp = new JScrollPane(jta,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        this.add(jsp, BorderLayout.CENTER);

	}

}
