package view;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;

import controller.ProfesorController;

public class DeleteDialogProfesor extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8375252376463263880L;

	public DeleteDialogProfesor(Frame parent) {
		super(parent, "Delete", true);
		
		setSize(250, 250);
		setLocationRelativeTo(parent);
		JButton btnOk = new JButton("Potvrda");
		JButton btnCancel = new JButton("Odustanak");
		this.setLayout(new FlowLayout());
		this.add(btnOk);
		this.add(btnCancel);
		
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				ProfesorController.getInstance().izbrisiProfesora();
				setVisible(false);
			}
		});
		
		
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		
		
		
		}	
}
