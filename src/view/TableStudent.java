package view;

import javax.swing.table.AbstractTableModel;

import model.BazaStudenata;
import model.Student;

public class TableStudent extends AbstractTableModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4668213724914277785L;
	private String[] columnNames = { "Ime", "Prezime","Godina rodjenja","Adresa","telefon","email","broj_indeksa","datum_upisa","godina_studija","Status","prosek" };
	
	private static TableStudent instance = null;

	public static TableStudent getInstance() {
		if (instance == null) {
			instance = new TableStudent();
		}
		return instance;
		
	}
	

	public TableStudent() {
	

	}


	public void setValueAt(Object value, int row, int col) {
		System.out.println("Postavio vrednost " + value + " na " + row + ","
				+ col + " (tipa " + value.getClass() + ")");
		Student student = BazaStudenata.getInstance().getStudent(row);
		student.set(col, value);
		BazaStudenata.getInstance().setStudent(row, student);
		// prijavimo da smo promenili vrednost u tabeli
		// fireTableCellUpdated(row, col);
	}
	

	public int getColumnCount() {
		return columnNames.length;
	}
	public int getRowCount() {
		return BazaStudenata.getInstance().size();
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}
	
	public Object getValueAt(int row, int col) {
		Student student = BazaStudenata.getInstance().getStudent(row);
		return student.toCell(col);
	}
	
	public Object getRow(int row) {
		Student student = BazaStudenata.getInstance().getStudent(row);
		return student;
	}

	
	/**
	 * Ako se ova metoda ne redefinise, koristi se default renderer/editor za
	 * celiju. To znaci da, ako je kolona tipa boolean, onda ce se u tabeli
	 * prikazati true/false, a ovako ce se za takav tip kolone pojaviti
	 * checkbox.
	 */
	public Class<?> getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}
	
	public boolean isCellEditable(int row, int col) {
		// Prva kolona ne moze da se menja
		if (col < 1) {
			return false;
		} else {
			return true;
		}
	}
	
	
	
	
}
