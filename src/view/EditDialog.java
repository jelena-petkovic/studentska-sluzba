package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import controller.StudentController;
import model.BazaStudenata;
import model.Status;
import model.Student;

public class EditDialog extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3274075939135739223L;

	@SuppressWarnings("unchecked")
	public EditDialog(Frame parent) {
		super(parent, "Edit", true);

		int row = Entiteti.getInstance().getSelectedRowStudent();
		Student student = BazaStudenata.getInstance().getStudent(row);
		
		
		setSize(250, 250);
		setLocationRelativeTo(parent);
	
		
		JPanel panCenter = new JPanel();
		BoxLayout boxCenter = new BoxLayout(panCenter, BoxLayout.Y_AXIS);
		panCenter.setLayout(boxCenter);

		// dimenzije labela i tekstualnih komponenti
		Dimension dim = new Dimension(150, 20);

		JPanel panIme = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblIme = new JLabel("Ime*");
		lblIme.setPreferredSize(dim);
		JTextField txtIme = new JTextField();
		txtIme.setText(student.getIme());
		txtIme.setPreferredSize(dim);
		panIme.add(lblIme);
		panIme.add(txtIme);
		
		
		JPanel panPrezime = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblPrezime = new JLabel("Prezime*");
		lblPrezime.setPreferredSize(dim);
		JTextField txtPrezime = new JTextField();
		txtPrezime.setText(student.getPrezime());
		txtPrezime.setPreferredSize(dim);
		panPrezime.add(lblPrezime);
		panPrezime.add(txtPrezime);

		JPanel panDatum = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblDatum = new JLabel("Datum rodjenja*");
		lblDatum.setPreferredSize(dim);
		JTextField txtDatum = new JTextField();
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
	
		txtDatum.setText(student.getDatum_rodjenja().format(formatter));
		txtDatum.setPreferredSize(dim);
		panDatum.add(lblDatum);
		panDatum.add(txtDatum);
		
		JPanel panAdresa = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblAdresa = new JLabel("Adresa stanovanja*");
		lblAdresa.setPreferredSize(dim);
		JTextField txtAdresa = new JTextField();
		txtAdresa.setText(student.getAdresa_stanovanja());
		txtAdresa.setPreferredSize(dim);
		panAdresa.add(lblAdresa);
		panAdresa.add(txtAdresa);
		
		JPanel panBroj = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblBroj = new JLabel("Broj telefona*");
		lblBroj.setPreferredSize(dim);
		JTextField txtBroj = new JTextField();
		txtBroj.setText(student.getKontakt_telefon());
		txtBroj.setPreferredSize(dim);
		panBroj.add(lblBroj);
		panBroj.add(txtBroj);
	
		JPanel panIndeks = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblIndeks = new JLabel("Broj indeksa*");
		lblIndeks.setPreferredSize(dim);
		JTextField txtIndeks = new JTextField();
		txtIndeks.setText(student.getBroj_indeksa());
		txtIndeks.setPreferredSize(dim);
		panIndeks.add(lblIndeks);
		panIndeks.add(txtIndeks);
	
		JPanel panGodStudija = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lblGodStudija = new JLabel("Trenutna godina studija*");
		lblGodStudija.setPreferredSize(dim);
		String[] items = { "I (prva)", "II (druga)", "III (treca)","IV (cetvrta)","V (peta)" };
		int[] itemsInt= {1,2,3,4,5};
		@SuppressWarnings("rawtypes")
		JComboBox c = new JComboBox();
		for (int i = 0; i < items.length; i++)
			c.addItem(items[i]);
		c.setSelectedIndex(student.getTrenutna_godina_studija()-1);
		panGodStudija.add(lblGodStudija);
		panGodStudija.add(c);
	


		JRadioButton budzet = new JRadioButton("Budzet");
		JRadioButton samofinansiranje = new JRadioButton("Samofinansiranje");
		ButtonGroup btnGroup1 = new ButtonGroup();
		btnGroup1.add(budzet);
		btnGroup1.add(samofinansiranje);
		
		if ( student.getStatus() == Status.B ) {
			budzet.setSelected(true);
		} else {
			samofinansiranje.setSelected(true);
		}

		JPanel panBudzet = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panBudzet.add(budzet);
		
		JPanel panSamofinansiranje = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panSamofinansiranje.add(samofinansiranje);
		
		
		panCenter.add(Box.createGlue());
		panCenter.add(panPrezime);
		panCenter.add(panIme);
		panCenter.add(panDatum);
		panCenter.add(panAdresa);
		panCenter.add(panBroj);
		panCenter.add(panIndeks);
		panCenter.add(panGodStudija);
		panCenter.add(panBudzet);
		panCenter.add(panSamofinansiranje);

		panCenter.add(Box.createVerticalStrut(25));
		add(panCenter, BorderLayout.CENTER);		
		
		JPanel panBottom = new JPanel();
		BoxLayout box = new BoxLayout(panBottom, BoxLayout.X_AXIS);
		panBottom.setLayout(box);

		JButton btnOk = new JButton("Potvrda");
		btnOk.setPreferredSize(new Dimension(100, 25));
		
		btnOk.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					

					Status status;
					
					 if (budzet.isSelected()) {
			                status=Status.B;
			        } else {
			        		status=Status.S;
			        }
					
					Student s = new Student(txtIme.getText(),txtPrezime.getText(),LocalDate.parse(txtDatum.getText(), formatter),txtAdresa.getText(), txtBroj.getText(), txtIndeks.getText(), itemsInt[c.getSelectedIndex()],status);
					
					StudentController.getInstance().izmeniStudenta(row, s);
					setVisible(false);
					
				}
		});
		
		

		JButton btnCancel = new JButton("Odustanak");
		btnCancel.setPreferredSize(new Dimension(100, 25));
		panBottom.add(Box.createGlue());
		panBottom.add(btnOk);
		panBottom.add(Box.createHorizontalStrut(10));
		panBottom.add(btnCancel);
		panBottom.add(Box.createHorizontalStrut(10));
		
		
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});

		add(panBottom, BorderLayout.SOUTH);
		pack();


	}
}
