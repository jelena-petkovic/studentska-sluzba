package view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.*;

public class StatusBar extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8294994708128116048L;

	public StatusBar() {
		this.setLayout(new BoxLayout(this,BoxLayout.X_AXIS));
		JLabel ssluzba = new JLabel("Studentska slu�ba");

		final JLabel vreme = new JLabel();
		final JLabel datum = new JLabel();
				
		final DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");  
	
		//vreme i datum
		ActionListener timerListener = new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                Date d = new Date();
                String time = timeFormat.format(d);
                String date = dateFormat.format(d);
                vreme.setText(time);
                datum.setText(date);
            }
        };
        Timer timer = new Timer(1000, timerListener);
        // to make sure it doesn't wait one second at the start
        timer.setInitialDelay(0);
        timer.start();
        
        add(ssluzba);
		add(Box.createGlue());
		add(vreme);
		add(Box.createHorizontalStrut(20)); 
		add(datum);
		add(Box.createHorizontalStrut(20)); 
		setBackground(Color.LIGHT_GRAY);
		
	}
}
